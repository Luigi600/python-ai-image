# Container
FROM python:3.9.5

WORKDIR /img-data

COPY ./ ./

RUN ["./pip-install.sh"]

# specifies what command to run within the container
CMD ["ls", "-al"] # debug
