#!/bin/bash

input="packages.txt"
while IFS= read -r line
do
	# echo "install $line"
	if [[ $line == scikit-learn* ]]
	then
		echo "install via U $line"
		pip install -U $line
	else
		echo "install via -I $line"
		pip install -I $line
	fi
done < "$input"